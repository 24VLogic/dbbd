import dbbd
import matplotlib.pyplot as plt
import numpy as np

def get_ud_map(bsd,save = False):
    ud_map_fig = plt.figure(facecolor = 'xkcd:grey',figsize = (6,6))
    ax1 = ud_map_fig.add_subplot(211)
    plt.imshow(bsd.ud_fr)
    ax2 = ud_map_fig.add_subplot(212)
    plt.imshow(bsd.ud_map)
    for line in bsd.ud_lines:
        plt.plot(line['x'],line['d'],color = 'r',alpha = 1,linewidth = 0.5)
    if save == True:
        ud_map_fig.savefig('dbbd_viewer/ud_map.png',facecolor = 'xkcd:grey')
    return ud_map_fig
def get_vd_map(bsd, save = False):
    vd_map_fig = plt.figure(facecolor = 'xkcd:grey',figsize = (6,6))
    ax1 = vd_map_fig.add_subplot(121)
    plt.imshow(bsd.vd_fr)
    ax2 = vd_map_fig.add_subplot(122)
    plt.imshow(bsd.vd_map)
    for line in bsd.vd_lines:
        plt.plot(line['d'],line['y'],color = 'r',alpha = 1,linewidth = 0.5)
    if save == True:
        vd_map_fig.savefig('dbbd_viewer/vd_map.png',facecolor = 'xkcd:grey')
    return vd_map_fig
def get_matched_lines_filtered(bsd, save = False):
    vd_map_fig = plt.figure(facecolor = 'xkcd:grey',figsize = (6,6))
    v_ax = vd_map_fig.add_subplot(122)
    plt.imshow(bsd.vd_map,cmap = 'gray')
    v_ax1 = vd_map_fig.add_subplot(121)
    v_ax1.imshow(bsd.vd_map, cmap = 'gray')
    ud_map_fig = plt.figure(facecolor = 'xkcd:grey',figsize = (6,6))
    u_ax = ud_map_fig.add_subplot(111)
    plt.imshow(bsd.ud_map,cmap = 'gray')
    dist_fig = plt.figure(facecolor = 'xkcd:grey',figsize = (6,6))
    dist_ax = dist_fig.add_subplot(111)
    dist_ax.imshow(bsd.distance_image,cmap = 'gray')
    dist_ax.imshow(bsd.amplitude_image,cmap = 'gray', alpha = 0.5)
    color=plt.cm.magma(np.linspace(0,1,len(bsd.uv_planes)))
    for col, plane in enumerate(bsd.uv_planes):
        u_vector = [plane['uu'][0][0],plane['uu'][0][-1]]
        v_vector = [plane['vv'].T[0][0],plane['vv'].T[0][-1]]
        if abs(u_vector[0]-u_vector[-1]) > 10:
            if abs(v_vector[0]-v_vector[-1]) > 10:
                v_ax.plot(plane['dd'].T[0],plane['vv'].T[0],color = color[col])
                u_ax.plot(plane['uu'][0],plane['dd'][0],color = color[col])
                dist_ax.plot(u_vector,[v_vector[0],v_vector[0]], color = color[col])
                dist_ax.plot(u_vector,[v_vector[-1],v_vector[-1]], color = color[col])
                dist_ax.plot([u_vector[0],u_vector[0]],v_vector, color = color[col])
                dist_ax.plot([u_vector[-1],u_vector[-1]],v_vector, color = color[col])

        #dist_ax.plot(plane['uu'].flatten(),plane['vv'].flatten(),'o', color = color[col], alpha = 0.5)
    if save == True:
        ud_map_fig.savefig('dbbd_viewer/ud_matched_lines_filtered.png',facecolor = 'xkcd:grey')
        vd_map_fig.savefig('dbbd_viewer/vd_matched_lines_filtered.png',facecolor = 'xkcd:grey')
        dist_fig.savefig('dbbd_viewer/dist_matched_lines_filtered.png', facecolor = 'xkcd:grey')
    return vd_map_fig, ud_map_fig
def get_matched_lines(bsd,save = False):
    vd_map_fig = plt.figure(facecolor = 'xkcd:grey',figsize = (6,6))
    v_ax = vd_map_fig.add_subplot(122)
    plt.imshow(bsd.vd_map,cmap = 'gray')
    v_ax1 = vd_map_fig.add_subplot(121)
    v_ax1.imshow(bsd.vd_map, cmap = 'gray')
    ud_map_fig = plt.figure(facecolor = 'xkcd:grey',figsize = (6,6))
    u_ax = ud_map_fig.add_subplot(212)
    u_ax1 = ud_map_fig.add_subplot(211)
    u_ax1.imshow(bsd.ud_map, cmap = 'gray')
    plt.imshow(bsd.ud_map,cmap = 'gray')
    dist_fig = plt.figure(facecolor = 'xkcd:grey',figsize = (6,6))
    dist_ax = dist_fig.add_subplot(111)
    dist_ax.imshow(bsd.distance_image,cmap = 'gray')
    dist_ax.imshow(bsd.amplitude_image,cmap = 'gray', alpha = 0.5)
    color=plt.cm.jet(np.linspace(0,1,len(bsd.uv_planes)))
    for col, plane in enumerate(bsd.uv_planes):
        u_vector = [plane['uu'][0][0],plane['uu'][0][-1]]
        v_vector = [plane['vv'].T[0][0],plane['vv'].T[0][-1]]
        v_ax.plot(plane['dd'].T[0],plane['vv'].T[0],color = color[col])
        u_ax.plot(plane['uu'][0],plane['dd'][0],color = color[col])
        dist_ax.plot(u_vector,[v_vector[0],v_vector[0]], color = color[col])
        dist_ax.plot(u_vector,[v_vector[-1],v_vector[-1]], color = color[col])
        dist_ax.plot([u_vector[0],u_vector[0]],v_vector, color = color[col])
        dist_ax.plot([u_vector[-1],u_vector[-1]],v_vector, color = color[col])

        #dist_ax.plot(plane['uu'].flatten(),plane['vv'].flatten(),'o', color = color[col], alpha = 0.5)
    if save == True:
        vd_map_fig.savefig('dbbd_viewer/vd_matched_lines.png',facecolor = 'xkcd:grey')
        ud_map_fig.savefig('dbbd_viewer/ud_matched_lines.png',facecolor = 'xkcd:grey')
        dist_fig.savefig('dbbd_viewer/dist_matched_lines.png',facecolor = 'xkcd:grey')

    return vd_map_fig, ud_map_fig, dist_fig

def get_matched_lines_filtered_d(bsd,d,save = False):
    vd_map_fig = plt.figure(facecolor = 'xkcd:grey',figsize = (6,6))
    v_ax = vd_map_fig.add_subplot(122)
    plt.imshow(bsd.vd_map,cmap = 'gray')
    v_ax1 = vd_map_fig.add_subplot(121)
    v_ax1.imshow(bsd.vd_map, cmap = 'gray')
    ud_map_fig = plt.figure(facecolor = 'xkcd:grey',figsize = (6,6))
    u_ax = ud_map_fig.add_subplot(111)
    plt.imshow(bsd.ud_map,cmap = 'gray')
    dist_fig = plt.figure(facecolor = 'xkcd:grey',figsize = (6,6))
    dist_ax = dist_fig.add_subplot(111)
    dist_ax.imshow(bsd.distance_image,cmap = 'gray')
    dist_ax.imshow(bsd.amplitude_image,cmap = 'gray', alpha = 0.5)
    color=plt.cm.jet(np.linspace(0,1,len(bsd.uv_planes)))
    for col, plane in enumerate(bsd.uv_planes):
        if plane['dd'][0][0] < d:
            u_vector = [plane['uu'][0][0],plane['uu'][0][-1]]
            v_vector = [plane['vv'].T[0][0],plane['vv'].T[0][-1]]
            v_ax.plot(plane['dd'].T[0],plane['vv'].T[0],color = color[col])
            u_ax.plot(plane['uu'][0],plane['dd'][0],color = color[col])
            dist_ax.plot(u_vector,[v_vector[0],v_vector[0]], color = color[col])
            dist_ax.plot(u_vector,[v_vector[-1],v_vector[-1]], color = color[col])
            dist_ax.plot([u_vector[0],u_vector[0]],v_vector, color = color[col])
            dist_ax.plot([u_vector[-1],u_vector[-1]],v_vector, color = color[col])

        #dist_ax.plot(plane['uu'].flatten(),plane['vv'].flatten(),'o', color = color[col], alpha = 0.5)
    if save == True:
        vd_map_fig.savefig('dbbd_viewer/vd_matched_lines_filtered_d.png',facecolor = 'xkcd:grey')
        ud_map_fig.savefig('dbbd_viewer/ud_matched_lines_filtered_d.png',facecolor = 'xkcd:grey')
        dist_fig.savefig('dbbd_viewer/dist_matched_lines_filtered_d.png',facecolor = 'xkcd:grey')
    return vd_map_fig, ud_map_fig

def get_yz_histogram_map(bsd, save = False):
    yz_map_figure = plt.figure()
    yz_ax = yz_map_figure.add_subplot(122)
    yz_ax.imshow(bsd.yz_ng)
    yz_ax1 = yz_map_figure.add_subplot(121)
    yz_ax1.imshow(bsd.yz_ng_fr,)
    for line in bsd.yz_ng_lines:
        yz_ax.plot(line['d'],line['y'], color = 'r',linewidth = 0.5)
    if save == True:
        yz_map_figure.savefig('dbbd_viewer/yz_map.png',facecolor = 'xkcd:grey')
    return yz_map_figure

def get_xz_histogram_map(bsd,save = False):
    xz_map_figure = plt.figure()
    xz_ax = xz_map_figure.add_subplot(212,aspect = 2)
    xz_ax.imshow(bsd.xz_ng)
    xz_ax1 = xz_map_figure.add_subplot(211,aspect = 2)
    xz_ax1.imshow(bsd.xz_ng_fr)
    for line in bsd.xz_ng_lines:
        xz_ax.plot(line['x'],line['d'], color = 'r',linewidth = 0.5)
    if save == True:
        xz_map_figure.savefig('dbbd_viewer/xz_map.png',facecolor = 'xkcd:grey')
    return xz_map_figure
def get_matched_hist(bsd,save = False):
    yz_map_figure = plt.figure(figsize =(6,6),facecolor = 'xkcd:grey')
    yz_ax = yz_map_figure.add_subplot(122)
    yz_ax1 = yz_map_figure.add_subplot(121)
    yz_ax1.imshow(bsd.yz_ng,cmap='gray')
    yz_ax.imshow(bsd.yz_ng,cmap = 'gray')
    xz_map_figure = plt.figure(figsize = (6,6),facecolor = 'xkcd:grey')
    xz_ax = xz_map_figure.add_subplot(212)
    xz_ax1 = xz_map_figure.add_subplot(211)
    xz_ax1.imshow(bsd.xz_ng,cmap='gray')
    xz_ax.imshow(bsd.xz_ng,cmap = 'gray')
    xy_mask_figure = plt.figure(figsize = (6,6),facecolor = 'xkcd:grey')
    mask_ax = xy_mask_figure.add_subplot(111)
    color=plt.cm.jet(np.linspace(0,1,len(bsd.xy_planes)))
    total_masks = np.full(bsd.distance_image.shape, 0)
    for col,plane in enumerate(bsd.xy_planes):
        yz_ax.plot(plane['zz'].T[0],plane['yy'].T[0],color = color[col])
        xz_ax.plot(plane['xx'][0],plane['zz'][0],color = color[col])

        total_masks += bsd.xy_plane_masks[col]
        #mask_ax.imshow(bsd.xy_plane_masks[i]*i)
    #for mask in bsd.xy_plane_masks:
        #total_masks += mask*bsd.amplitude_image
    #print(len(bsd.xy_plane_masks)-len(bsd.xy_planes))
    total_masks = total_masks.astype(float)
    #total_masks[np.where(total_masks == 0)]=np.nan
    mask_ax.imshow(bsd.distance_image,cmap='gray_r')
    mask_ax.imshow(total_masks,alpha = 0.5)
    if save == True:
        yz_map_figure.savefig('dbbd_viewer/yz_map_matched_lines.png',facecolor = 'grey')
        xz_map_figure.savefig('dbbd_viewer/xz_map_matched_lines.png',facecolor = 'grey')
        xy_mask_figure.savefig('dbbd_viewer/xy_matched_masks.png',facecolor = 'grey')

    return yz_map_figure, xz_map_figure

def main():
    #data = np.load('data/testData3a.npy')
    #data = np.load('data/new_test_data4a.npy')
    data = np.load('data/new_test_data_5b.npy')
    bsd1 = dbbd.bsdata(data)
    bsd1.get_ground_mask()
    bsd1.blur_distance_image
    bsd1.ud_bins = 40
    bsd1.vd_bins = 40
    bsd1.get_uv_analysis()
    get_ud_map(bsd1,save = True)
    get_vd_map(bsd1,save = True)
    get_matched_lines(bsd1,save = True)
    bsd1.get_xyz_histogram_analysis()
    get_yz_histogram_map(bsd1,save = True)
    get_xz_histogram_map(bsd1,save = True)
    plt.imshow(bsd1.xy_plane_masks[10])
    get_matched_hist(bsd1,save = True)
    plt.tight_layout()
    plt.show()
if __name__ == '__main__':
    main()
